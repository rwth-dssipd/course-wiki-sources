# DSSIPD SoSe 2021 Course Wiki Website

## These are the source files

This repo contains the source files for the DSSIPD website hosted on _GitLab Pages_ [here](https://rwth-dssipd.gitlab.io/course-wiki/).

## How to use this repository

This repo can be used by students to add content to the wiki using the _Publii_ Desktop CMS program.

### What is the Publii Desktop CMS program?

It is a desktop software that you can download and install on zour Windows, Mac OS X, or Linux operating system, which will allow you to edit the contents of the website via friendly user interface.

### Where to download from?

This is where zou should download Publii: [https://getpublii.com/download/](https://getpublii.com/download/)

### What to do after downloading?

After downloading and installing Publii, use the _SmartGit_ program to clone this repository into Publii's sites folder, this is usually located in your documents folder under `Publii/sites`.

After cloning, all you need is to run the _Publii desktop CMS_ program to edit the wiki.

### What do I do to publish the changes?

In order to publish the changes, please follow these steps:

a. Stage and commit the changes in the sources repo
a. Obtain a GitLab API token from your GitLab account
a. Enter the token into the site settings in Publii and sync the website

